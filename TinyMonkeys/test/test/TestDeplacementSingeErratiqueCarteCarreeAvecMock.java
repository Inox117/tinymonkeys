package test;

import junit.framework.TestCase;
import org.easymock.EasyMock;
import org.easymock.TestSubject;
import org.junit.Before;
import org.junit.Test;
import tinymonkeys.modele.BandeDeSingesErratiques;
import tinymonkeys.modele.Ile;
import tinymonkeys.modele.Pirate;
import tinymonkeys.modele.SingeErratique;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Classe de test avec une île standard bouchonnée
 * @author : Quentin Victoor
 * @version : 1
 */
public class TestDeplacementSingeErratiqueCarteCarreeAvecMock extends TestCase
{

    /**
     * Constructeur.
     */
    public TestDeplacementSingeErratiqueCarteCarreeAvecMock() {  }

    /**
     * l'île.
     */
    private Ile ile;

    /**
     * le singe, sur lequel les tests vont être focalisés.
     */
    private SingeErratique singe;

    /**
     * position X du singe.
     */
    private int positionX = 10;

    /**
     * position Y du singe.
     */
    private int positionY = 10;


    /**
     * Déclaration des variables
     */
    @Before
    public void setUp() throws Exception
    {
        this.ile = EasyMock.createMock(Ile.class);

        this.singe = new SingeErratique(this.positionX,this.positionY,this.ile);

    }

    // Test des déplacements possibles
    /**
     * On test qu'il est possible de déplacer un singe sachant qu'il n'y a aucun autre singe
     * et que les 4 déplacements sont possibles.
     * Les 4 déplacements sont possibles
     */
    @Test
    public void testDeplacementSingePossibleSansAutreSinge()
    {
                EasyMock.expect(this.ile.getLargeurCarte()).andStubReturn(20);
                EasyMock.expect(this.ile.getLongueurCarte()).andStubReturn(20);
                EasyMock.expect(this.ile.getSingesErratiques()).andStubReturn(new BandeDeSingesErratiques(this.ile));
                EasyMock.expect(this.ile.isCaseTerre(11, 10)).andStubReturn(true);
                EasyMock.expect(this.ile.isCaseTerre(9, 10)).andStubReturn(true);
                EasyMock.expect(this.ile.isCaseTerre(10, 11)).andStubReturn(true);
                EasyMock.expect(this.ile.isCaseTerre(10, 9)).andStubReturn(true);
                EasyMock.replay(this.ile);

                this.test4DeplacementPossible(true,true,true,true);
    }

    /**
     * On test qu'il est possible de déplacer un singe sachant qu'il y a un autre singe qui ne bloque pas
     * et que les 4 déplacements sont possibles.
     */
    @Test
    public void testDeplacementSingePossibleAvecAutreSingeNonBloquant()
    {
                BandeDeSingesErratiques bande = new BandeDeSingesErratiques(this.ile);
                SingeErratique autreSinge = new SingeErratique(1, 1, this.ile);
                bande.ajoutSinge(autreSinge);
                EasyMock.expect(this.ile.getLargeurCarte()).andStubReturn(20);
                EasyMock.expect(this.ile.getLongueurCarte()).andStubReturn(20);
                EasyMock.expect(this.ile.getSingesErratiques()).andStubReturn(bande);
                EasyMock.expect(this.ile.isCaseTerre(11, 10)).andStubReturn(true);
                EasyMock.expect(this.ile.isCaseTerre(9, 10)).andStubReturn(true);
                EasyMock.expect(this.ile.isCaseTerre(10, 11)).andStubReturn(true);
                EasyMock.expect(this.ile.isCaseTerre(10, 9)).andStubReturn(true);
                EasyMock.replay(this.ile);


                this.test4DeplacementPossible(true,true,true,true);
    }

    /**
     * On test qu'il est possible de déplacer un singe dans 3 direction car la 4ème est bloqué par un singe
     */
    @Test
    public void testDeplacementSingeAvecSingeBloquant()
    {
                BandeDeSingesErratiques bande = new BandeDeSingesErratiques(this.ile);
                SingeErratique autreSinge = new SingeErratique(11, 10, this.ile);
                bande.ajoutSinge(autreSinge);
                EasyMock.expect(this.ile.getLargeurCarte()).andStubReturn(20);
                EasyMock.expect(this.ile.getLongueurCarte()).andStubReturn(20);
                EasyMock.expect(this.ile.getSingesErratiques()).andStubReturn(bande);
                EasyMock.expect(this.ile.isCaseTerre(11, 10)).andStubReturn(true);
                EasyMock.expect(this.ile.isCaseTerre(9, 10)).andStubReturn(true);
                EasyMock.expect(this.ile.isCaseTerre(10, 11)).andStubReturn(true);
                EasyMock.expect(this.ile.isCaseTerre(10, 9)).andStubReturn(true);
                EasyMock.replay(this.ile);

                this.test4DeplacementPossible(true,true,false,true);
    }

    /**
     * On test qu'il est possible de déplacer un singe dans 3 direction car la 4ème est bloqué par de l'eau
     */
    @Test
    public void testDeplacementSingeAvecEauBloquant()
    {
                EasyMock.expect(this.ile.getLargeurCarte()).andStubReturn(20);
                EasyMock.expect(this.ile.getLongueurCarte()).andStubReturn(20);
                EasyMock.expect(this.ile.getSingesErratiques()).andStubReturn(new BandeDeSingesErratiques(this.ile));
                EasyMock.expect(this.ile.isCaseTerre(11, 10)).andStubReturn(false);
                EasyMock.expect(this.ile.isCaseTerre(9, 10)).andStubReturn(true);
                EasyMock.expect(this.ile.isCaseTerre(10, 11)).andStubReturn(true);
                EasyMock.expect(this.ile.isCaseTerre(10, 9)).andStubReturn(true);
                EasyMock.replay(this.ile);

                this.test4DeplacementPossible(true,true,false,true);
    }

    /**
     * On test que si aucun des mouvements n'est possible le singe ne bouge pas
     */
    @Test
    public void testAucunDeplacement()
    {
        Pirate pirate = EasyMock.createMock(Pirate.class);
        EasyMock.expect(pirate.getX()).andStubReturn(1);
        EasyMock.expect(pirate.getY()).andStubReturn(1);
        EasyMock.replay(pirate);

        EasyMock.expect(this.ile.getLargeurCarte()).andStubReturn(20);
        EasyMock.expect(this.ile.getLongueurCarte()).andStubReturn(20);
        EasyMock.expect(this.ile.getSingesErratiques()).andStubReturn(new BandeDeSingesErratiques(this.ile));
        EasyMock.expect(this.ile.isCaseTerre(11, 10)).andStubReturn(false);
        EasyMock.expect(this.ile.isCaseTerre(9, 10)).andStubReturn(false);
        EasyMock.expect(this.ile.isCaseTerre(10, 11)).andStubReturn(false);
        EasyMock.expect(this.ile.isCaseTerre(10, 9)).andStubReturn(false);
        EasyMock.expect(this.ile.isCaseTerre(this.positionX, this.positionY)).andReturn(true);
        EasyMock.expect(this.ile.getPirate()).andStubReturn(pirate);
        EasyMock.replay(this.ile);

        this.test4DeplacementPossible(false,false,false,false);
        this.singe.deplacerSinge();
        assertTrue("X a change", this.singe.getX() == this.positionX);
        assertTrue("Y a change", this.singe.getY()==this.positionY);
    }

    /**
     * On test que le pirate est manger si le singe va dessus
     */
    @Test
    public void testSingeMangePirate()
    {
        Pirate pirate = EasyMock.createMock(Pirate.class);
        EasyMock.expect(pirate.getX()).andStubReturn(11);
        EasyMock.expect(pirate.getY()).andStubReturn(10);
        pirate.isMort();
        EasyMock.expectLastCall().andReturn(true);
        pirate.informerMort();
        EasyMock.expectLastCall().once();
        EasyMock.replay(pirate);

        BandeDeSingesErratiques bande = new BandeDeSingesErratiques(this.ile);
        bande.ajoutSinge(this.singe);

        EasyMock.expect(this.ile.getLargeurCarte()).andStubReturn(20);
        EasyMock.expect(this.ile.getLongueurCarte()).andStubReturn(20);
        EasyMock.expect(this.ile.getSingesErratiques()).andStubReturn(bande);
        EasyMock.expect(this.ile.isCaseTerre(11, 10)).andStubReturn(true);
        EasyMock.expect(this.ile.isCaseTerre(9, 10)).andStubReturn(false);
        EasyMock.expect(this.ile.isCaseTerre(10, 11)).andStubReturn(false);
        EasyMock.expect(this.ile.isCaseTerre(10, 9)).andStubReturn(false);
        EasyMock.expect(this.ile.isCaseTerre(this.positionX, this.positionY)).andReturn(true);
        EasyMock.expect(this.ile.getPirate()).andStubReturn(pirate);
        EasyMock.replay(this.ile);

        this.test4DeplacementPossible(false,false,true,false);
        this.singe.deplacerSinge();
        assertTrue("Le pirate n'est pas mort", pirate.isMort());
    }

    /**
     * On test que le pirate n'est pas manger si le singe va dessus
     */
    @Test
    public void testSingeNeMangePasPirate()
    {
        Pirate pirate = EasyMock.createMock(Pirate.class);
        EasyMock.expect(pirate.getX()).andStubReturn(11);
        EasyMock.expect(pirate.getY()).andStubReturn(10);
        pirate.isMort();
        EasyMock.expectLastCall().andReturn(false);
        EasyMock.replay(pirate);

        BandeDeSingesErratiques bande = new BandeDeSingesErratiques(this.ile);
        bande.ajoutSinge(this.singe);

        EasyMock.expect(this.ile.getLargeurCarte()).andStubReturn(20);
        EasyMock.expect(this.ile.getLongueurCarte()).andStubReturn(20);
        EasyMock.expect(this.ile.getSingesErratiques()).andStubReturn(bande);
        EasyMock.expect(this.ile.isCaseTerre(11, 10)).andStubReturn(false);
        EasyMock.expect(this.ile.isCaseTerre(9, 10)).andStubReturn(true);
        EasyMock.expect(this.ile.isCaseTerre(10, 11)).andStubReturn(true);
        EasyMock.expect(this.ile.isCaseTerre(10, 9)).andStubReturn(true);
        EasyMock.expect(this.ile.isCaseTerre(this.positionX, this.positionY)).andReturn(true);
        EasyMock.expect(this.ile.getPirate()).andStubReturn(pirate);
        EasyMock.replay(this.ile);

        this.test4DeplacementPossible(true,true,false,true);
        this.singe.deplacerSinge();
        assertFalse("Le pirate est mort", pirate.isMort());
    }


    /** Appel à assertTrue pour le déplacement d'un singe selon dx et dy.
     * @param dx dx
     * @param dy dy
     */
    private void testDeplacementBon(int dx, int dy)
    {
        assertTrue(" Deplacement impossible (" + dx + " " + dy + ") alors que les coordonnées sont OK",
                this.singe.validationDeplacement(dx, dy));
    }

    /**
     * Appel à assertTrue pour le déplacement d'un singe selon dx et dy.
     * @param dx dx
     * @param dy dy
     */
    private void testDeplacementMauvais(int dx, int dy)
    {
        assertFalse(" Deplacement possible (" + dx + " " + dy + ") alors que les coordonnées sont incorrecte",
                this.singe.validationDeplacement(dx, dy));
    }

    /**
     * Méthode permettant de tester les 4 déplacements possibles pour un singe.
     * @param isUpPossible possibilité d'aller en haut
     * @param isDownPossible possibilité d'aller en bas
     * @param isRightPossible possibilité d'aller à droite
     * @param isLeftPossible possibilité d'aller à gauche
     */
    private void test4DeplacementPossible(boolean isUpPossible,
                                          boolean isDownPossible,
                                          boolean isRightPossible,
                                          boolean isLeftPossible)
    {
        if (isUpPossible)
        {
            this.testDeplacementBon(0, -1);
        }else
        {
            this.testDeplacementMauvais(0, -1);
        }
        if (isDownPossible)
        {
            this.testDeplacementBon(0, 1);
        }else
        {
            this.testDeplacementMauvais(0, 1);
        }
        if (isRightPossible)
        {
            this.testDeplacementBon(1, 0);
        }else
        {
            this.testDeplacementMauvais(1, 0);
        }
        if (isLeftPossible)
        {
            this.testDeplacementBon(-1, 0);
        }else
        {
            this.testDeplacementMauvais(-1, 0);
        }
    }

}

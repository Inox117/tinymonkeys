package test;

import org.junit.Before;
import org.junit.Test;

import tinymonkeys.controleur.Controleur;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;

import tinymonkeys.modele.BandeDeSingesErratiques;
import tinymonkeys.modele.Ile;
import tinymonkeys.modele.SingeErratique;

/**
 * Classe de test avec une île standard.
 * @author : Quentin Victoor
 * @version : 1
 */
public class TestDeplacementSingeErratiqueCarteCarreeSansMock
{
    /**
     * Constructeur vide.
     */
    public TestDeplacementSingeErratiqueCarteCarreeSansMock()
    {
    }

    /**
     * l'île
     */
    private Ile ile;

    /**
     * le singe, sur lequel les tests vont être focalisés.
     */
    private SingeErratique singe;

    /**
     * La bande de singes.
     */
    private BandeDeSingesErratiques bandeDeSingesErratiques;

    /**
     * Déclaration des variables.
     */
    @Before
    public void setUp() throws Exception
    {
                this.ile = new Ile();
                this.ile.setCarte(Controleur.exempleCarte());
                this.singe = new SingeErratique(10, 10, this.ile);
                this.bandeDeSingesErratiques = new BandeDeSingesErratiques(this.ile);

    }

    /**
     * On test qu'il est possible de déplacer le singe dans les 4 directions.
     */
    @Test
    public void testDeplacementSingePossibleSansAutreSingeEtSansEau()
    {
                // centre
                this.singe.setPosition(10, 10);
                this.test4DeplacementPossible(true, true, true, true);
    }

    // Test des déplacements possibles
    /**
     * On test qu'il est possible de déplacer un singe sachant qu'il n'y a aucun autre singe.
     * On test différentes situation.
     */
    @Test
    public void testDeplacementSingePossibleSansAutreSingeEtAvecEau()
    {


                // coin haut gauche
                this.singe.setPosition(1, 1);
                this.test4DeplacementPossible(false, true, true, false);

                // cote gauche
                this.singe.setPosition(1, 10);
                this.test4DeplacementPossible(true, true, true, false);

                // coin bas gauche
                this.singe.setPosition(1, 18);
                this.test4DeplacementPossible(true, false, true, false);

                // milieu haut
                this.singe.setPosition(10, 1);
                this.test4DeplacementPossible(false, true, true, true);

                // milieu bas
                this.singe.setPosition(10, 18);
                this.test4DeplacementPossible(true, false, true, true);

                // coin haut droit
                this.singe.setPosition(18, 1);
                this.test4DeplacementPossible(false, true, false, true);

                // milieu droit
                this.singe.setPosition(18, 10);
                this.test4DeplacementPossible(true, true, false, true);

                // coin bas droit
                this.singe.setPosition(18, 18);
                this.test4DeplacementPossible(true, false, false, true);
    }

    /**
     * On test qu'il est possible de déplacer un singe dans le coin haut gauche en fonction des singes autours de lui.
     */
    @Test
    public void testDeplacementSingeCoinHautGaucheAvecAutreSingeEtEau()
    {
                //Singe dans le coin haut gauche
                this.singe.setPosition(1, 1);
                this.bandeDeSingesErratiques = new BandeDeSingesErratiques(this.ile);
                final SingeErratique singeHorizontal = new SingeErratique(1, 2, this.ile);
                this.bandeDeSingesErratiques.ajoutSinge(singeHorizontal);
                this.ile.setErratiques(this.bandeDeSingesErratiques);
                this.test4DeplacementPossible(false, false, true, false);

                this.bandeDeSingesErratiques = new BandeDeSingesErratiques(this.ile);
                final SingeErratique singeVertical = new SingeErratique(2, 1, this.ile);
                this.bandeDeSingesErratiques.ajoutSinge(singeVertical);
                this.ile.setErratiques(this.bandeDeSingesErratiques);
                this.test4DeplacementPossible(false, true, false, false);

                this.bandeDeSingesErratiques = new BandeDeSingesErratiques(this.ile);
                this.bandeDeSingesErratiques.ajoutSinge(singeHorizontal);
                this.bandeDeSingesErratiques.ajoutSinge(singeVertical);
                this.ile.setErratiques(this.bandeDeSingesErratiques);
                this.test4DeplacementPossible(false, false, false, false);

    }

    /**
     * On test qu'il est possible de déplacer un singe dans le coin bas gauche en fonction des singes autours de lui.
     */
    @Test
    public void testDeplacementSingeCoinBasGaucheAvecAutreSingeEtEau()
    {
                //Singe dans le coin bas gauche
                this.singe.setPosition(1, 18);
                this.bandeDeSingesErratiques = new BandeDeSingesErratiques(this.ile);
                final SingeErratique singeVertical = new SingeErratique(1, 17, this.ile);
                this.bandeDeSingesErratiques.ajoutSinge(singeVertical);
                this.ile.setErratiques(this.bandeDeSingesErratiques);
                this.test4DeplacementPossible(false, false, true, false);

                this.bandeDeSingesErratiques = new BandeDeSingesErratiques(this.ile);
                final SingeErratique singeHorizontal = new SingeErratique(2, 18, this.ile);
                this.bandeDeSingesErratiques.ajoutSinge(singeHorizontal);
                this.ile.setErratiques(this.bandeDeSingesErratiques);
                this.test4DeplacementPossible(true, false, false, false);

                this.bandeDeSingesErratiques = new BandeDeSingesErratiques(this.ile);
                this.bandeDeSingesErratiques.ajoutSinge(singeVertical);
                this.bandeDeSingesErratiques.ajoutSinge(singeHorizontal);
                this.ile.setErratiques(this.bandeDeSingesErratiques);
                this.test4DeplacementPossible(false, false, false, false);
    }

    /**
     * On test qu'il est possible de déplacer un singe dans le coin haut droite en fonction des singes autours de lui.
     */
    @Test
    public void testDeplacementSingeCoinHautDroiteAvecAutreSingeEtEau()
    {
                //Singe dans le coin haut droite
                this.singe.setPosition(18, 1);
                this.bandeDeSingesErratiques = new BandeDeSingesErratiques(this.ile);
                final SingeErratique singeVertical = new SingeErratique(17, 1, this.ile);
                this.bandeDeSingesErratiques.ajoutSinge(singeVertical);
                this.ile.setErratiques(this.bandeDeSingesErratiques);
                this.test4DeplacementPossible(false, true, false, false);

                this.bandeDeSingesErratiques = new BandeDeSingesErratiques(this.ile);
                final SingeErratique singeHorizontal = new SingeErratique(18, 2, this.ile);
                this.bandeDeSingesErratiques.ajoutSinge(singeHorizontal);
                this.ile.setErratiques(this.bandeDeSingesErratiques);
                this.test4DeplacementPossible(false, false, false, true);

                this.bandeDeSingesErratiques = new BandeDeSingesErratiques(this.ile);
                this.bandeDeSingesErratiques.ajoutSinge(singeVertical);
                this.bandeDeSingesErratiques.ajoutSinge(singeHorizontal);
                this.ile.setErratiques(this.bandeDeSingesErratiques);
                this.test4DeplacementPossible(false, false, false, false);
    }

    /**
     * On test qu'il est possible de déplacer un singe dans le coin bas droite en fonction des singes autours de lui.
     */
    @Test
    public void testDeplacementSingeCoinBasDroiteAvecAutreSingeEtEau()
    {
                //Singe dans le coin bas droite
                this.singe.setPosition(18, 18);
                this.bandeDeSingesErratiques = new BandeDeSingesErratiques(this.ile);
                final SingeErratique singeVertical = new SingeErratique(17, 18, this.ile);
                this.bandeDeSingesErratiques.ajoutSinge(singeVertical);
                this.ile.setErratiques(this.bandeDeSingesErratiques);
                this.test4DeplacementPossible(true, false, false, false);

                this.bandeDeSingesErratiques = new BandeDeSingesErratiques(this.ile);
                final SingeErratique singeHorizontal = new SingeErratique(18, 17, this.ile);
                this.bandeDeSingesErratiques.ajoutSinge(singeHorizontal);
                this.ile.setErratiques(this.bandeDeSingesErratiques);
                this.test4DeplacementPossible(false, false, false, true);

                this.bandeDeSingesErratiques = new BandeDeSingesErratiques(this.ile);
                this.bandeDeSingesErratiques.ajoutSinge(singeVertical);
                this.bandeDeSingesErratiques.ajoutSinge(singeHorizontal);
                this.ile.setErratiques(this.bandeDeSingesErratiques);
                this.test4DeplacementPossible(false, false, false, false);
    }

    /**
     * On test qu'il est possible de déplacer un singe dans le milieu gauche en fonction des singes autours de lui.
     */
    @Test
    public void testDeplacementSingeMilieuHautAvecAutreSingeEtEau()
    {
                //milieu haut
                this.singe.setPosition(10, 1);
                final SingeErratique singeVertical = new SingeErratique(10, 2, this.ile);
                final SingeErratique singeHorizontalGauche = new SingeErratique(9, 1, this.ile);
                final SingeErratique singeHorizontalDroite = new SingeErratique(11, 1, this.ile);

                this.testDeplacement1SingeMilieuHaut(singeVertical, singeHorizontalGauche, singeHorizontalDroite);
                this.testDeplacement2SingesMilieuHaut(singeVertical, singeHorizontalGauche, singeHorizontalDroite);
                this.testDeplacement3Singes(singeVertical, singeHorizontalGauche, singeHorizontalDroite);
    }

    /**
     * On test qu'il est possible de déplacer un singe dans le milieu gauche en fonction des singes autours de lui.
     */
    @Test
    public void testDeplacementSingeMilieuBasAvecAutreSingeEtEau()
    {
                //milieu haut
                this.singe.setPosition(10, 18);
                final SingeErratique singeVertical = new SingeErratique(10, 17, this.ile);
                final SingeErratique singeHorizontalGauche = new SingeErratique(9, 18, this.ile);
                final SingeErratique singeHorizontalDroite = new SingeErratique(11, 18, this.ile);

                this.testDeplacement1SingeMilieuBas(singeVertical, singeHorizontalGauche, singeHorizontalDroite);
                this.testDeplacement2SingesMilieuBas(singeVertical, singeHorizontalGauche, singeHorizontalDroite);
                this.testDeplacement3Singes(singeVertical, singeHorizontalGauche, singeHorizontalDroite);
    }

    /**
     * On test qu'il est possible de déplacer un singe dans le milieu gauche en fonction des singes autours de lui
     */
    @Test
    public void testDeplacementMilieuGaucheAvecAutreSingeEau()
    {
                //milieu gauche
                this.singe.setPosition(1, 10);
                final SingeErratique singeHorizontal = new SingeErratique(2, 10, this.ile);
                final SingeErratique singeVerticalHaut = new SingeErratique(1, 9, this.ile);
                final SingeErratique singeVerticalBas = new SingeErratique(1, 11, this.ile);

                this.testDeplacement1SingeMilieuGauche(singeHorizontal, singeVerticalHaut, singeVerticalBas);
                this.testDeplacement2SingesMilieuGauche(singeHorizontal, singeVerticalHaut, singeVerticalBas);
                this.testDeplacement3Singes(singeHorizontal, singeVerticalHaut, singeVerticalBas);
    }

    /**
     * On test qu'il est possible de déplacer un singe dans le milieu gauche en fonction des singes autours de lui
     */
    @Test
    public void testDeplacementMilieuDroiteAvecAutreSingeEau()
    {
                //milieu droite
                this.singe.setPosition(18,10);
                final SingeErratique singeHorizontal = new SingeErratique(17, 10, this.ile);
                final SingeErratique singeVerticalHaut = new SingeErratique(18, 9, this.ile);
                final SingeErratique singeVerticalBas = new SingeErratique(18, 11, this.ile);

                this.testDeplacement1SingeMilieuDroite(singeHorizontal, singeVerticalHaut, singeVerticalBas);
                this.testDeplacement2SingesMilieuDroite(singeHorizontal, singeVerticalHaut, singeVerticalBas);
                this.testDeplacement3Singes(singeHorizontal, singeVerticalHaut, singeVerticalBas);
    }

    @Test
    public void  testDeplacementCentreAvecAutreSinge()
    {
                //centre
                this.singe.setPosition(10, 10);
                final SingeErratique singeVerticalHaut = new SingeErratique(10, 9, this.ile);
                final SingeErratique singeVerticalBas = new SingeErratique(10, 11, this.ile);
                final SingeErratique singeHorizontalGauche = new SingeErratique(9, 10, this.ile);
                final SingeErratique singeHorizontalDroite = new SingeErratique(11, 10, this.ile);
                this.testDeplacement1SingeCentre(singeVerticalHaut, singeVerticalBas, singeHorizontalGauche, singeHorizontalDroite);
                this.testDeplacement2SingeCentre(singeVerticalHaut, singeVerticalBas, singeHorizontalGauche, singeHorizontalDroite);
                this.testDeplacement3SingeCentre(singeVerticalHaut, singeVerticalBas, singeHorizontalGauche, singeHorizontalDroite);
                this.testDeplacement4SingeCentre(singeVerticalHaut, singeVerticalBas, singeHorizontalGauche, singeHorizontalDroite);
    }

    /** Appel à assertTrue pour le déplacement d'un singe selon dx et dy.
     * @param dx dx
     * @param dy dy
     */
    private void testDeplacementBon(int dx, int dy)
    {
             assertTrue(" Deplacement impossible (" + dx + " " + dy + ") alors que les coordonnées sont OK",
                this.singe.validationDeplacement(dx, dy));
    }

    /**
     * Appel à assertTrue pour le déplacement d'un singe selon dx et dy.
     * @param dx dx
     * @param dy dy
     */
    private void testDeplacementMauvais(int dx, int dy)
    {
                assertFalse(" Deplacement possible (" + dx + " " + dy + ") alors que les coordonnées sont incorrecte",
                this.singe.validationDeplacement(dx, dy));
    }

    /**
     * Méthode pour tester les déplacements du singe positionné au milieu haut avec 1 singe à coté.
     * @param singeVertical singeVertical
     * @param singeHorizontalGauche singeHorizontalGauche
     * @param singeHorizontalDroite singeHorizontalDroite
     */
    private void testDeplacement1SingeMilieuHaut(SingeErratique singeVertical,
                                                 SingeErratique singeHorizontalGauche,
                                                 SingeErratique singeHorizontalDroite)
    {
                this.bandeDeSingesErratiques = new BandeDeSingesErratiques(this.ile);
                this.bandeDeSingesErratiques.ajoutSinge(singeVertical);
                this.ile.setErratiques(this.bandeDeSingesErratiques);
                this.test4DeplacementPossible(false, false, true, true);

                this.bandeDeSingesErratiques = new BandeDeSingesErratiques(this.ile);
                this.bandeDeSingesErratiques.ajoutSinge(singeHorizontalGauche);
                this.ile.setErratiques(this.bandeDeSingesErratiques);
                this.test4DeplacementPossible(false, true, true, false);

                this.bandeDeSingesErratiques = new BandeDeSingesErratiques(this.ile);
                this.bandeDeSingesErratiques.ajoutSinge(singeHorizontalDroite);
                this.ile.setErratiques(this.bandeDeSingesErratiques);
                this.test4DeplacementPossible(false, true, false, true);

    }

    /**
     * Méthode pour tester les déplacements du singe positionné au milieu haut avec 1 singe à coté.
     * @param singeVertical singeVertical
     * @param singeHorizontalGauche singeHorizontalGauche
     * @param singeHorizontalDroite singeHorizontalDroite
     */
    private void testDeplacement1SingeMilieuBas(SingeErratique singeVertical,
                                                SingeErratique singeHorizontalGauche,
                                                SingeErratique singeHorizontalDroite)
    {
                this.bandeDeSingesErratiques = new BandeDeSingesErratiques(this.ile);
                this.bandeDeSingesErratiques.ajoutSinge(singeVertical);
                this.ile.setErratiques(this.bandeDeSingesErratiques);
                this.test4DeplacementPossible(false, false, true, true);

                this.bandeDeSingesErratiques = new BandeDeSingesErratiques(this.ile);
                this.bandeDeSingesErratiques.ajoutSinge(singeHorizontalGauche);
                this.ile.setErratiques(this.bandeDeSingesErratiques);
                this.test4DeplacementPossible(true, false, true, false);

                this.bandeDeSingesErratiques = new BandeDeSingesErratiques(this.ile);
                this.bandeDeSingesErratiques.ajoutSinge(singeHorizontalDroite);
                this.ile.setErratiques(this.bandeDeSingesErratiques);
                this.test4DeplacementPossible(true, false, false, true);

    }

    /**
     * Méthode pour tester les déplacements du singe positionné au milieu haut avec 2 singe à coté.
     * @param singeVertical singe vertical
     * @param singeHorizontalGauche singe horizontal gauche
     * @param singeHorizontalDroite singe horizontal droite
     */
    private void testDeplacement2SingesMilieuHaut(SingeErratique singeVertical,
                                                  SingeErratique singeHorizontalGauche,
                                                  SingeErratique singeHorizontalDroite)
    {
                this.testDeplacementMilieuHautOuBas1SingeVerticalEt1SingeHorizontale(singeVertical, singeHorizontalGauche, singeHorizontalDroite);

                this.bandeDeSingesErratiques = new BandeDeSingesErratiques(this.ile);
                this.bandeDeSingesErratiques.ajoutSinge(singeHorizontalGauche);
                this.bandeDeSingesErratiques.ajoutSinge(singeHorizontalDroite);
                this.ile.setErratiques(this.bandeDeSingesErratiques);
                this.test4DeplacementPossible(false, true, false, false);
    }

    /**
     * Méthode pour tester les déplacements du singe positionné au milieu bas avec 2 singe à coté.
     * @param singeVertical singe vertical
     * @param singeHorizontalGauche singe horizontal gauche
     * @param singeHorizontalDroite singe horizontal droite
     */
    private void testDeplacement2SingesMilieuBas(SingeErratique singeVertical,
                                                 SingeErratique singeHorizontalGauche,
                                                 SingeErratique singeHorizontalDroite)
    {
                this.testDeplacementMilieuHautOuBas1SingeVerticalEt1SingeHorizontale(singeVertical, singeHorizontalGauche, singeHorizontalDroite);

                this.bandeDeSingesErratiques = new BandeDeSingesErratiques(this.ile);
                this.bandeDeSingesErratiques.ajoutSinge(singeHorizontalGauche);
                this.bandeDeSingesErratiques.ajoutSinge(singeHorizontalDroite);
                this.ile.setErratiques(this.bandeDeSingesErratiques);
                this.test4DeplacementPossible(true, false, false, false);
    }

    /**
     * Méthode pour tester les déplacements du singe positionné au milieu haut avec 3 singes à coté.
     * @param singe1 singe1
     * @param singe2 singe2
     * @param singe3 singe3
     */
    private void testDeplacement3Singes(SingeErratique singe1,
                                        SingeErratique singe2,
                                        SingeErratique singe3)
    {
                this.bandeDeSingesErratiques = new BandeDeSingesErratiques(this.ile);
                this.bandeDeSingesErratiques.ajoutSinge(singe1);
                this.bandeDeSingesErratiques.ajoutSinge(singe2);
                this.bandeDeSingesErratiques.ajoutSinge(singe3);
                this.ile.setErratiques(this.bandeDeSingesErratiques);
                this.test4DeplacementPossible(false, false, false, false);
    }

    /**
     * Méthode pour tester les deplacement en fonction de la position d'un singe vertical et d'un singe horizontal.
     * Pour les milieu haut et bas
     * @param singeVertical singe vertical
     * @param singeHorizontalGauche singe horizontal gauche
     * @param singeHorizontalDroite singe horizontal droite
     */
    private void testDeplacementMilieuHautOuBas1SingeVerticalEt1SingeHorizontale(SingeErratique singeVertical,
                                                                                 SingeErratique singeHorizontalGauche,
                                                                                 SingeErratique singeHorizontalDroite)
    {
                this.bandeDeSingesErratiques = new BandeDeSingesErratiques(this.ile);
                this.bandeDeSingesErratiques.ajoutSinge(singeVertical);
                this.bandeDeSingesErratiques.ajoutSinge(singeHorizontalGauche);
                this.ile.setErratiques(this.bandeDeSingesErratiques);
                this.test4DeplacementPossible(false, false, true, false);

                this.bandeDeSingesErratiques = new BandeDeSingesErratiques(this.ile);
                this.bandeDeSingesErratiques.ajoutSinge(singeVertical);
                this.bandeDeSingesErratiques.ajoutSinge(singeHorizontalDroite);
                this.ile.setErratiques(this.bandeDeSingesErratiques);
                this.test4DeplacementPossible(false, false, false, true);
    }

    /**
     * Méthode pour tester les déplacements du singe positionné au milieu gauche avec 1 singe à coté.
     * @param singeHorizontal singe horizontal
     * @param singeVerticalHaut singe vertical haut
     * @param singeVerticalBas singe vertical bas
     */
    private void testDeplacement1SingeMilieuGauche(SingeErratique singeHorizontal,
                                                   SingeErratique singeVerticalHaut,
                                                   SingeErratique singeVerticalBas)
    {
                this.bandeDeSingesErratiques = new BandeDeSingesErratiques(this.ile);
                this.bandeDeSingesErratiques.ajoutSinge(singeHorizontal);
                this.ile.setErratiques(this.bandeDeSingesErratiques);
                this.test4DeplacementPossible(true, true, false, false);

                this.bandeDeSingesErratiques = new BandeDeSingesErratiques(this.ile);
                this.bandeDeSingesErratiques.ajoutSinge(singeVerticalHaut);
                this.ile.setErratiques(this.bandeDeSingesErratiques);
                this.test4DeplacementPossible(false, true, true, false);

                this.bandeDeSingesErratiques = new BandeDeSingesErratiques(this.ile);
                this.bandeDeSingesErratiques.ajoutSinge(singeVerticalBas);
                this.ile.setErratiques(this.bandeDeSingesErratiques);
                this.test4DeplacementPossible(true, false, true, false);
    }

    /**
     * Méthode pour tester les déplacements du singe positionné au milieu gauche avec 1 singe à coté.
     * @param singeHorizontal singe horizontal
     * @param singeVerticalHaut singe vertical haut
     * @param singeVerticalBas singe vertical bas
     */
    private void testDeplacement1SingeMilieuDroite(SingeErratique singeHorizontal,
                                                   SingeErratique singeVerticalHaut,
                                                   SingeErratique singeVerticalBas)
    {
                this.bandeDeSingesErratiques = new BandeDeSingesErratiques(this.ile);
                this.bandeDeSingesErratiques.ajoutSinge(singeHorizontal);
                this.ile.setErratiques(this.bandeDeSingesErratiques);
                this.test4DeplacementPossible(true, true, false, false);

                this.bandeDeSingesErratiques = new BandeDeSingesErratiques(this.ile);
                this.bandeDeSingesErratiques.ajoutSinge(singeVerticalHaut);
                this.ile.setErratiques(this.bandeDeSingesErratiques);
                this.test4DeplacementPossible(false, true, false, true);

                this.bandeDeSingesErratiques = new BandeDeSingesErratiques(this.ile);
                this.bandeDeSingesErratiques.ajoutSinge(singeVerticalBas);
                this.ile.setErratiques(this.bandeDeSingesErratiques);
                this.test4DeplacementPossible(true, false, false, true);
    }

    /**
     * Méthode pour tester les déplacements du singe positionné au milieu gauche avec 2 singe à coté.
     * @param singeHorizontal singe horizontal
     * @param singeVerticalHaut singe vertical haut
     * @param singeVerticalBas singe vertical bas
     */
    private void testDeplacement2SingesMilieuGauche(SingeErratique singeHorizontal,
                                                    SingeErratique singeVerticalHaut,
                                                    SingeErratique singeVerticalBas)
    {
                this.testDeplacementMilieuGaucheOuDroite1SingeVerticalEt1SingeHorizontale(singeHorizontal, singeVerticalHaut, singeVerticalBas);

                this.bandeDeSingesErratiques = new BandeDeSingesErratiques(this.ile);
                this.bandeDeSingesErratiques.ajoutSinge(singeVerticalHaut);
                this.bandeDeSingesErratiques.ajoutSinge(singeVerticalBas);
                this.ile.setErratiques(this.bandeDeSingesErratiques);
                this.test4DeplacementPossible(false, false, true, false);
    }

    /**
     * Méthode pour tester les déplacements du singe positionné au milieu droite avec 2 singe à coté.
     * @param singeHorizontal singe horizontal
     * @param singeVerticalHaut singe vertical haut
     * @param singeVerticalBas singe vertical bas
     */
    private void testDeplacement2SingesMilieuDroite(SingeErratique singeHorizontal,
                                                    SingeErratique singeVerticalHaut,
                                                    SingeErratique singeVerticalBas)
    {
                this.testDeplacementMilieuGaucheOuDroite1SingeVerticalEt1SingeHorizontale(singeHorizontal, singeVerticalHaut, singeVerticalBas);

                this.bandeDeSingesErratiques = new BandeDeSingesErratiques(this.ile);
                this.bandeDeSingesErratiques.ajoutSinge(singeVerticalHaut);
                this.bandeDeSingesErratiques.ajoutSinge(singeVerticalBas);
                this.ile.setErratiques(this.bandeDeSingesErratiques);
                this.test4DeplacementPossible(false, false, false, true);
    }

    /**
     * Méthode pour tester les deplacement en fonction de la position d'un singe vertical et d'un singe horizontal.
     * Pour les milieu gauche et droite
     * @param singeHorizontal singe horizontal
     * @param singeVerticalHaut singe vertical haut
     * @param singeVerticalBas singe vertical bas
     */
    private void testDeplacementMilieuGaucheOuDroite1SingeVerticalEt1SingeHorizontale(SingeErratique singeHorizontal, SingeErratique singeVerticalHaut, SingeErratique singeVerticalBas) {

                this.bandeDeSingesErratiques = new BandeDeSingesErratiques(this.ile);
                this.bandeDeSingesErratiques.ajoutSinge(singeHorizontal);
                this.bandeDeSingesErratiques.ajoutSinge(singeVerticalHaut);
                this.ile.setErratiques(this.bandeDeSingesErratiques);
                this.test4DeplacementPossible(false, true, false, false);

                this.bandeDeSingesErratiques = new BandeDeSingesErratiques(this.ile);
                this.bandeDeSingesErratiques.ajoutSinge(singeHorizontal);
                this.bandeDeSingesErratiques.ajoutSinge(singeVerticalBas);
                this.ile.setErratiques(this.bandeDeSingesErratiques);
                this.test4DeplacementPossible(true, false, false, false);

    }

    /**
     * Méthode pour tester les deplacements du singe au centre en fonction des autres singes.
     * @param singeVerticalHaut singe vertical haut
     * @param singeVerticalBas singe vertical bas
     * @param singeHorizontalGauche singe horizontal gauche
     * @param singeHorizontalDroite singe horizontal droite
     */
    private void testDeplacement1SingeCentre(SingeErratique singeVerticalHaut,
                                             SingeErratique singeVerticalBas,
                                             SingeErratique singeHorizontalGauche,
                                             SingeErratique singeHorizontalDroite)
    {
                this.bandeDeSingesErratiques = new BandeDeSingesErratiques(this.ile);
                this.bandeDeSingesErratiques.ajoutSinge(singeVerticalHaut);
                this.ile.setErratiques(this.bandeDeSingesErratiques);
                this.test4DeplacementPossible(false, true, true, true);

                this.bandeDeSingesErratiques = new BandeDeSingesErratiques(this.ile);
                this.bandeDeSingesErratiques.ajoutSinge(singeVerticalBas);
                this.ile.setErratiques(this.bandeDeSingesErratiques);
                this.test4DeplacementPossible(true, false, true, true);

                this.bandeDeSingesErratiques = new BandeDeSingesErratiques(this.ile);
                this.bandeDeSingesErratiques.ajoutSinge(singeHorizontalDroite);
                this.ile.setErratiques(this.bandeDeSingesErratiques);
                this.test4DeplacementPossible(true, true, false, true);

                this.bandeDeSingesErratiques = new BandeDeSingesErratiques(this.ile);
                this.bandeDeSingesErratiques.ajoutSinge(singeHorizontalGauche);
                this.ile.setErratiques(this.bandeDeSingesErratiques);
                this.test4DeplacementPossible(true, true, true, false);

    }

    /**
     * Méthode pour tester les deplacements du singe au centre en fonction des autres singes.
     * @param singeVerticalHaut singe vertical haut
     * @param singeVerticalBas singe vertical bas
     * @param singeHorizontalGauche singe horizontal gauche
     * @param singeHorizontalDroite singe horizontal droite
     */
    private void testDeplacement2SingeCentre(SingeErratique singeVerticalHaut,
                                             SingeErratique singeVerticalBas,
                                             SingeErratique singeHorizontalGauche,
                                             SingeErratique singeHorizontalDroite)
    {
        // haut gauche
        this.bandeDeSingesErratiques = new BandeDeSingesErratiques(this.ile);
        this.bandeDeSingesErratiques.ajoutSinge(singeVerticalHaut);
        this.bandeDeSingesErratiques.ajoutSinge(singeHorizontalGauche);
        this.ile.setErratiques(this.bandeDeSingesErratiques);
        this.test4DeplacementPossible(false, true, true, false);

        // haut bas
        this.bandeDeSingesErratiques = new BandeDeSingesErratiques(this.ile);
        this.bandeDeSingesErratiques.ajoutSinge(singeVerticalHaut);
        this.bandeDeSingesErratiques.ajoutSinge(singeVerticalBas);
        this.ile.setErratiques(this.bandeDeSingesErratiques);
        this.test4DeplacementPossible(false, false, true, true);

        // haut droite
        this.bandeDeSingesErratiques = new BandeDeSingesErratiques(this.ile);
        this.bandeDeSingesErratiques.ajoutSinge(singeVerticalHaut);
        this.bandeDeSingesErratiques.ajoutSinge(singeHorizontalDroite);
        this.ile.setErratiques(this.bandeDeSingesErratiques);
        this.test4DeplacementPossible(false, true, false, true);

        // gauche droite
        this.bandeDeSingesErratiques = new BandeDeSingesErratiques(this.ile);
        this.bandeDeSingesErratiques.ajoutSinge(singeHorizontalGauche);
        this.bandeDeSingesErratiques.ajoutSinge(singeHorizontalDroite);
        this.ile.setErratiques(this.bandeDeSingesErratiques);
        this.test4DeplacementPossible(true, true, false, false);

        // bas gauche
        this.bandeDeSingesErratiques = new BandeDeSingesErratiques(this.ile);
        this.bandeDeSingesErratiques.ajoutSinge(singeVerticalBas);
        this.bandeDeSingesErratiques.ajoutSinge(singeHorizontalGauche);
        this.ile.setErratiques(this.bandeDeSingesErratiques);
        this.test4DeplacementPossible(true, false, true, false);

        // bas droite
        this.bandeDeSingesErratiques = new BandeDeSingesErratiques(this.ile);
        this.bandeDeSingesErratiques.ajoutSinge(singeVerticalBas);
        this.bandeDeSingesErratiques.ajoutSinge(singeHorizontalDroite);
        this.ile.setErratiques(this.bandeDeSingesErratiques);
        this.test4DeplacementPossible(true, false, false, true);
    }

    /**
     * Méthode pour tester les deplacements du singe au centre en fonction des autres singes.
     * @param singeVerticalHaut singe vertical haut
     * @param singeVerticalBas singe vertical bas
     * @param singeHorizontalGauche singe horizontal gauche
     * @param singeHorizontalDroite singe horizontal droite
     */
    private void testDeplacement3SingeCentre(SingeErratique singeVerticalHaut,
                                             SingeErratique singeVerticalBas,
                                             SingeErratique singeHorizontalGauche,
                                             SingeErratique singeHorizontalDroite)
    {
        int ancientX = this.singe.getX();
        int ancientY = this.singe.getY();
        // haut
        this.bandeDeSingesErratiques = new BandeDeSingesErratiques(this.ile);
        this.bandeDeSingesErratiques.ajoutSinge(singeVerticalBas);
        this.bandeDeSingesErratiques.ajoutSinge(singeHorizontalGauche);
        this.bandeDeSingesErratiques.ajoutSinge(singeHorizontalDroite);
        this.ile.setErratiques(this.bandeDeSingesErratiques);
        this.test4DeplacementPossible(true, false, false, false);
        this.singe.deplacerSinge();
        assertTrue("Deplacement Incorrect HAUT X", this.singe.getX() == ancientX);
        assertTrue("Deplacement Incorrect HAUT Y", (this.singe.getY()==ancientY-1||this.singe.getY()==ancientY));
        this.singe.setPosition(ancientX, ancientY);

        // bas
        this.bandeDeSingesErratiques = new BandeDeSingesErratiques(this.ile);
        this.bandeDeSingesErratiques.ajoutSinge(singeVerticalHaut);
        this.bandeDeSingesErratiques.ajoutSinge(singeHorizontalGauche);
        this.bandeDeSingesErratiques.ajoutSinge(singeHorizontalDroite);
        this.ile.setErratiques(this.bandeDeSingesErratiques);
        this.test4DeplacementPossible(false, true, false, false);
        this.singe.deplacerSinge();
        assertTrue("Deplacement Incorrect BAS X", this.singe.getX() == ancientX);
        assertTrue("Deplacement Incorrect BAS Y",(this.singe.getY()==ancientY+1||this.singe.getY()==ancientY));
        this.singe.setPosition(ancientX, ancientY);

        // gauche
        this.bandeDeSingesErratiques = new BandeDeSingesErratiques(this.ile);
        this.bandeDeSingesErratiques.ajoutSinge(singeVerticalHaut);
        this.bandeDeSingesErratiques.ajoutSinge(singeVerticalBas);
        this.bandeDeSingesErratiques.ajoutSinge(singeHorizontalDroite);
        this.ile.setErratiques(this.bandeDeSingesErratiques);
        this.test4DeplacementPossible(false, false, false, true);
        this.singe.deplacerSinge();
        assertTrue("Deplacement Incorrect GAUCHE X",(this.singe.getX()==ancientX-1||this.singe.getX()==ancientX));
        assertTrue("Deplacement Incorrect GAUCHE Y",this.singe.getY()==ancientY);
        this.singe.setPosition(ancientX, ancientY);

        // droite
        this.bandeDeSingesErratiques = new BandeDeSingesErratiques(this.ile);
        this.bandeDeSingesErratiques.ajoutSinge(singeVerticalHaut);
        this.bandeDeSingesErratiques.ajoutSinge(singeVerticalBas);
        this.bandeDeSingesErratiques.ajoutSinge(singeHorizontalGauche);
        this.ile.setErratiques(this.bandeDeSingesErratiques);
        this.test4DeplacementPossible(false, false, true, false);
        this.singe.deplacerSinge();
        assertTrue("Deplacement Incorrect DROITE X",(this.singe.getX()==ancientX+1||this.singe.getX()==ancientX));
        assertTrue("Deplacement Incorrect DROITE Y",this.singe.getY()==ancientY);
        this.singe.setPosition(ancientX, ancientY);
    }

    /**
     * Méthode pour tester les deplacements du singe au centre en fonction des autres singes.
     * @param singeVerticalHaut singe vertical haut
     * @param singeVerticalBas singe vertical bas
     * @param singeHorizontalGauche singe horizontal gauche
     * @param singeHorizontalDroite singe horizontal droite
     */
    private void testDeplacement4SingeCentre(SingeErratique singeVerticalHaut,
                                             SingeErratique singeVerticalBas,
                                             SingeErratique singeHorizontalGauche,
                                             SingeErratique singeHorizontalDroite)
    {

                this.bandeDeSingesErratiques = new BandeDeSingesErratiques(this.ile);
                this.bandeDeSingesErratiques.ajoutSinge(singeVerticalHaut);
                this.bandeDeSingesErratiques.ajoutSinge(singeVerticalBas);
                this.bandeDeSingesErratiques.ajoutSinge(singeHorizontalGauche);
                this.bandeDeSingesErratiques.ajoutSinge(singeHorizontalDroite);
                this.ile.setErratiques(this.bandeDeSingesErratiques);
                this.test4DeplacementPossible(false, false, false, false);

    }

    /**
     * Méthode permettant de tester les 4 déplacements possibles pour un singe.
     * @param isUpPossible possibilité d'aller en haut
     * @param isDownPossible possibilité d'aller en bas
     * @param isRightPossible possibilité d'aller à droite
     * @param isLeftPossible possibilité d'aller à gauche
     */
    private void test4DeplacementPossible(boolean isUpPossible,
                                          boolean isDownPossible,
                                          boolean isRightPossible,
                                          boolean isLeftPossible)
    {
        if (isUpPossible)
        {
            this.testDeplacementBon(0, -1);
        }else
        {
            this.testDeplacementMauvais(0, -1);
        }
        if (isDownPossible)
        {
            this.testDeplacementBon(0, 1);
        }else
        {
            this.testDeplacementMauvais(0, 1);
        }
        if (isRightPossible)
        {
            this.testDeplacementBon(1, 0);
        }else
        {
            this.testDeplacementMauvais(1, 0);
        }
        if (isLeftPossible)
        {
            this.testDeplacementBon(-1, 0);
        }else
        {
            this.testDeplacementMauvais(-1, 0);
        }
    }

}

package test;

import org.junit.Before;
import org.junit.Test;
import tinymonkeys.controleur.Controleur;
import tinymonkeys.modele.Ile;
import tinymonkeys.modele.SingeErratique;

import static org.junit.Assert.assertEquals;

/**
 * Classe de test d'équiprobabilité avec une île standard.
 * @author : Quentin Victoor
 * @version : 1
 */
public class TestDeplacementSingeErratiqueEquiprobabilite {

    /**
     * Nombre de boucle pour tester l'équiprobabilité
     */
    private final static int NBTEST = 100;

    /**
     * Précision de l'équiprobabilité fixée ici à 5%
     */
    private final static double PRECISION = 0.5;

    /**
     * l'île
     */
    private Ile ile;

    /**
     * le singe, sur lequel les tests vont être focalisés.
     */
    private SingeErratique singe;

    /**
     * la position x du singe.
     */
    private int singeX;

    /**
     * la position y du singe.
     */
    private int singeY;

    /**
     * Nombre de déplacement en haut
     */
    private int nbHaut;

    /**
     * Nombre de déplacement en bas
     */
    private int nbBas;

    /**
     * Nombre de déplacement à droite
     */
    private int nbDroite;

    /**
     * Nombre de déplacement à gauche
     */
    private int nbGauche;

    /**
     * Constructeur vide
     */
    public TestDeplacementSingeErratiqueEquiprobabilite() {
    }

    private void testEquiproba()
    {
        this.nbHaut = 0;
        this.nbBas = 0;
        this.nbDroite = 0;
        this.nbGauche = 0;
        for(int i = 0; i < NBTEST; i++){
            this.singe.deplacerSinge();
            if(this.singe.getX() == (this.singeX - 1) && this.singe.getY() == this.singeY){
                this.nbGauche++;
            }else if(this.singe.getX() == (this.singeX + 1) && this.singe.getY() == this.singeY){
                this.nbDroite++;
            }else if(this.singe.getX() == this.singeX && this.singe.getY() == (this.singeY - 1)){
                this.nbHaut++;
            }else if(this.singe.getX() == this.singeX && this.singe.getY() == (this.singeY + 1)){
                this.nbBas++;
            }
            this.singe.setPosition(this.singeX, this.singeY);
        }
    }

    /**
     * initialisation des variables
     * @throws Exception
     */
    @Before
    public void setUp() throws Exception
    {
        this.ile = new Ile();
        this.ile.setCarte(Controleur.exempleCarte());
        this.singe = new SingeErratique(10, 10, this.ile);
        this.singeX = this.singe.getX();
        this.singeY = this.singe.getY();
    }

    @Test
    public void testEqui4DeplacementPossible()
    {
        this.testEquiproba();
        int nbDirection = 4;
        assertEquals(NBTEST / nbDirection, this.nbHaut, NBTEST * PRECISION);
        assertEquals(NBTEST / nbDirection, this.nbBas, NBTEST * PRECISION);
        assertEquals(NBTEST / nbDirection, this.nbGauche, NBTEST * PRECISION);
        assertEquals(NBTEST / nbDirection, this.nbDroite, NBTEST * PRECISION);

    }

    @Test
    public void testEqui3DeplacementPossible()
    {
        int nbDirection = 3;

        this.singe.setPosition(1, 10);
        this.testEquiproba();
        assertEquals(NBTEST / nbDirection, this.nbHaut, NBTEST * PRECISION);
        assertEquals(NBTEST / nbDirection, this.nbBas, NBTEST * PRECISION);
        assertEquals(0, this.nbGauche, NBTEST * PRECISION);
        assertEquals(NBTEST / nbDirection, this.nbDroite, NBTEST * PRECISION);

        this.singe.setPosition(18, 10);
        this.testEquiproba();
        assertEquals(NBTEST / nbDirection, this.nbHaut, NBTEST * PRECISION);
        assertEquals(NBTEST / nbDirection, this.nbBas, NBTEST * PRECISION);
        assertEquals(NBTEST / nbDirection, this.nbGauche, NBTEST * PRECISION);
        assertEquals(0, this.nbDroite, NBTEST * PRECISION);

        this.singe.setPosition(10, 1);
        this.testEquiproba();
        assertEquals(0, this.nbHaut, NBTEST * PRECISION);
        assertEquals(NBTEST / nbDirection, this.nbBas, NBTEST * PRECISION);
        assertEquals(NBTEST / nbDirection, this.nbGauche, NBTEST * PRECISION);
        assertEquals(NBTEST / nbDirection, this.nbDroite, NBTEST * PRECISION);

        this.singe.setPosition(10, 18);
        this.testEquiproba();
        assertEquals(NBTEST / nbDirection, this.nbHaut, NBTEST * PRECISION);
        assertEquals(0, this.nbBas, NBTEST * PRECISION);
        assertEquals(NBTEST / nbDirection, this.nbGauche, NBTEST * PRECISION);
        assertEquals(NBTEST / nbDirection, this.nbDroite, NBTEST * PRECISION);
    }

    @Test
    public void testEqui2DeplacementPossible()
    {
        int nbDirection = 2;

        this.singe.setPosition(1, 1);
        this.testEquiproba();
        assertEquals(0, this.nbHaut, NBTEST * PRECISION);
        assertEquals(NBTEST / nbDirection, this.nbBas, NBTEST * PRECISION);
        assertEquals(0, this.nbGauche, NBTEST * PRECISION);
        assertEquals(NBTEST / nbDirection, this.nbDroite, NBTEST * PRECISION);

        this.singe.setPosition(18, 1);
        this.testEquiproba();
        assertEquals(0, this.nbHaut, NBTEST * PRECISION);
        assertEquals(NBTEST / nbDirection, this.nbBas, NBTEST * PRECISION);
        assertEquals(NBTEST / nbDirection, this.nbGauche, NBTEST * PRECISION);
        assertEquals(0, this.nbDroite, NBTEST * PRECISION);

        this.singe.setPosition(1, 18);
        this.testEquiproba();
        assertEquals(NBTEST / nbDirection, this.nbHaut, NBTEST * PRECISION);
        assertEquals(0, this.nbBas, NBTEST * PRECISION);
        assertEquals(0, this.nbGauche, NBTEST * PRECISION);
        assertEquals(NBTEST / nbDirection, this.nbDroite, NBTEST * PRECISION);

        this.singe.setPosition(18, 18);
        this.testEquiproba();
        assertEquals(NBTEST / nbDirection, this.nbHaut, NBTEST * PRECISION);
        assertEquals(0, this.nbBas, NBTEST * PRECISION);
        assertEquals(NBTEST / nbDirection, this.nbGauche, NBTEST * PRECISION);
        assertEquals(0, this.nbDroite, NBTEST * PRECISION);
    }
}

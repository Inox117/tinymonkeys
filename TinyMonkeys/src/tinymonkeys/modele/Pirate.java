package tinymonkeys.modele;

import java.util.Random;
import javax.swing.event.EventListenerList;

/**
 * Classe d'un pirate. 
 * 
 * @version 1.0
 * @author Camille Constant
 *
 */
public class Pirate 
{

	/**
	 * Le chemin vers l'image du pirate.
	 */
	private String avatar;
	
	/**
	 * Coordonnee en abscisse du pirate (indice dans la carte).
	 */
	private int x;
	
	/**
	 * Coordonnee en ordonnee du pirate (indice dans la carte).
	 */
	private int y;
	
	/**
	 * Ile aux singes.
	 */
	private Ile monkeyIsland;
	
	/**
	 * Liste des écouteurs sur le pirate.
	 */
	final private EventListenerList pirateEcouteurs;

	/**
	 * Vie du pirate.
	 */
	private boolean mort;
	/**
	 * Constructeur du pirate sans position ni nom renseignes mais avec l'ile associee.
	 * 
	 * @param ile l'ile contenant tous les elements de celle-ci.
	 */
	public Pirate(Ile ile)
	{
		this.monkeyIsland = ile;
		this.pirateEcouteurs = new EventListenerList();
	}
	
	/**
	 * Constructeur du pirate avec le nom renseigne.
	 * 
	 * @param ile l'ile contenant tous les elements de celle-ci.
	 * @param avatar le lien vers l'avatar du pirate.
	 */
	public Pirate(Ile ile, String avatar)
	{
		this.monkeyIsland = ile;
		this.avatar = avatar;
		this.pirateEcouteurs = new EventListenerList();
	}
		
	/**
	 * Accesseur en lecture de la position en abscisse du pirate.
	 * @return la coordonnee en abscisse.
	 */
	public int getX()
	{
		return this.x;
	}
	
	
	/**
	 * Accesseur en lecture de la position en ordonnee du pirate.
	 * @return la coordonnee en ordonnee.
	 */
	public int getY()
	{
		return this.y;
	}
	
	
	/**
	 * Place le pirate a sa position initiale.
	 * 
	 * @param x la coordonnee initiale en abscisse.
	 * @param y la coordonnee initiale en ordonnee.
	 */
	public void positionInitiale(int x, int y)
	{	
		//TODO
		this.x = x;
		this.y = y;
		for( PirateEcouteur e :this.pirateEcouteurs.getListeners(PirateEcouteur.class)){
			e.ajoutPirate(0, this.x, this.y, this.getAvatar());
			e.liberationClavier();
		}
	}
	
	/**
	 * Methode deplacant le pirate selon la direction demandee.
	 * Si le deplacement indique positionne le pirate sur un singe, le pirate meurt.
	 * Si le deplacement indique positionne le pirate sur le tresor, le tresor est detruit.
	 * 
	 * @param dx la direction en abscisse (comprise entre -1 et 1).
	 * @param dy la direction en ordonnee (comprise entre -1 et 1).
	 */
	public void demandeDeplacement(int dx, int dy)
	{
		//TODO
		if (!this.mort) {
			if (this.monkeyIsland.getCarte()[this.x + dx][this.y + dy] == 1) {
				this.x += dx;
				this.y += dy;
			}
			this.gererPositionTresor();
			this.gererPositionSinges();
		}
	}

	/**
	 * Permet de vérifier la position du pirate vis à vis du trésor.
	 */
	private void gererPositionTresor()
	{
		if (this.monkeyIsland.getTresor().coordonneesEgales(this.getX(), this.getY()))
		{
			this.monkeyIsland.suppressionTresor();
			this.monkeyIsland.creationTresor();
		}
	}

	/**
	 * Accesseur en écriture des listeners du pirate.
	 *
	 * @return ecouteurs
	 */
	public EventListenerList getPirateEcouteurs()
	{
		return this.pirateEcouteurs;
	}

	/**
	 * Permet de vérifier la position du pirate vis à vis des singes.
	 */
	private void gererPositionSinges()
	{
		for (SingeErratique singe : this.monkeyIsland.getSingesErratiques().getSingesErratiques()) {
			if (singe.coordonneesEgales(this.getX(), this.getY())) {
				this.mort = true;
				break;
			}
		}
		for (PirateEcouteur ec : this.pirateEcouteurs.getListeners(PirateEcouteur.class))
		{
			if (this.mort) {
				ec.mortPirate(0);
				this.setMort(true);
			} else {
				ec.deplacementPirate(0, this.getX(), this.getY());
				ec.liberationClavier();
			}
		}
	}


	/**
	 * Accesseur en lecture de l'avatar.
	 * 
	 * @return le chemin vers l'image.
	 */
	public String getAvatar()
	{
		return this.avatar;
	}
	
	/**
	 * Accesseur en ecriture de l'avatar du pirate.
	 * 
	 * @param avatar l'avatar du pirate.
	 */
	public void setAvatar(String avatar)
	{
		this.avatar = avatar;
	}
		
	
	/**
	 * Enregistre dans la liste des ecouteurs de pirate l'ecouteur passe en parametre.
	 * 
	 * @param ecouteur ecouteur du pirate.
	 */
	public void enregistreEcPirate(PirateEcouteur ecouteur)
	{
		this.pirateEcouteurs.add(PirateEcouteur.class, ecouteur);
	}

	/**
	 * Choix d'une position initiale aléatoire.
	 */
	public void setPositionInitiale()
	{
		final Random randX = new Random();
		final Random randY = new Random();
		int x;
		int y;
		do {
			x = randX.nextInt(this.monkeyIsland.getLargeurCarte());
			y = randY.nextInt(this.monkeyIsland.getLongueurCarte());
		} while (this.monkeyIsland.valeurCarte(x, y) == 0 );
		this.positionInitiale(x, y);
	}

	/**
	 * Accesseur en lecture de la vie du pirate.
	 * @return mort
	 */
	public boolean isMort()
	{
		return this.mort;
	}

	/**
	 * Accesseur en ecriture de la vie du pirate.
	 * @param mort vie du pirate
	 */
	public void setMort(boolean mort)
	{
		this.mort = mort;
	}

    public void informerMort(){
        for (PirateEcouteur ec : this.getPirateEcouteurs().getListeners(PirateEcouteur.class))
        {
                this.setMort(true);
                ec.mortPirate(0);
        }
    }
}

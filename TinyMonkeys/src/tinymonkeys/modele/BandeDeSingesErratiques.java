package tinymonkeys.modele;

import java.util.Random;
import java.util.Vector;

import javax.swing.event.EventListenerList;

/**
 * Classe d'une bande de singes erratiques.
 * 
 * @version 1.0
 * @author Camille Constant
 *
 */
public class BandeDeSingesErratiques extends Thread
{
	
	/**
	 * Vecteur contenant l'ensemble des singes erratiques.
	 */
	private Vector<SingeErratique> erratiques;
	
	/**
	 * L'ile.
	 */
	private Ile monkeyIsland;
	
	/**
	 * Liste des écouteurs sur la bande de singes erratiques.
	 */
	final private EventListenerList bandeSingesEcouteurs;
	
	/**
	 * Constructeur d'une bande de singes erratiques vide.
	 * 
	 * @param ile l'ile contenant l'ensemble des elements de celle-ci.
	 */
	public BandeDeSingesErratiques(Ile ile)
	{
		super();
		this.erratiques = new Vector<SingeErratique>();
		this.monkeyIsland = ile;
		this.bandeSingesEcouteurs = new EventListenerList();
	}
	
	/**
	 * Accesseur en lecture a l'ensemble des singes erratiques.
	 * 
	 * @return le vecteur de singes erratiques.
	 */
	public Vector<SingeErratique> getSingesErratiques()
	{
		return this.erratiques;
	}

	
	/**
	 * Ajout du nombre indique de singes erratiques a des positions libres aleatoires.
	 * 
	 * @param n le nombre de singes a ajouter.
	 */
	public void ajoutSingesErratiques(int n)
	{
		// TODO
		SingeErratique singe;
		for (int i = 0; i < n; i++) {
			final Random randX = new Random();
			final Random randY = new Random();
			int x;
			int y;
			do {
				x = randX.nextInt(this.monkeyIsland.getLargeurCarte());
				y = randY.nextInt(this.monkeyIsland.getLongueurCarte());
			} while (this.monkeyIsland.getCarte()[x][y] == 0
					|| x == this.monkeyIsland.getPirate().getX()
					|| y == this.monkeyIsland.getPirate().getY()
					|| this.isPositionOccupeeParAutreSinge(x, y));
			singe = new SingeErratique(x, y, this.monkeyIsland);
			this.ajoutSinge(singe);
		}
	}

	/**
	 * ajout d'un singe à la bande.
	 * @param singe singe à ajouter
	 */
	public void ajoutSinge(SingeErratique singe)
	{
		this.erratiques.add(singe);
		for (BandeDeSingesErratiquesEcouteur ec: this.bandeSingesEcouteurs
				.getListeners(BandeDeSingesErratiquesEcouteur.class)) {
			ec.creationSingeErratique(this.erratiques.indexOf(singe), singe.getX(), singe.getY());
		}
	}

	/**
	 * Vérifie que la position du singe que l'on va crée n'est pas déjà utilisée.
	 * @param x abscisse de la position
	 * @param y ordonnée de la position
	 * @return liberté de la case
	 */
	private boolean isPositionOccupeeParAutreSinge(int x, int y)
	{
		boolean libre = false;
		for (SingeErratique singe : this.erratiques) {
			if (singe.coordonneesEgales(x, y)) {
				libre = true;
			}
		}
		return libre;
	}


	/**
	 * Enregistre dans la liste des ecouteurs de bande de singes l'ecouteur passe en parametre.
	 * @param ecouteur ecouteur de la bande de singes.
	 */
	public void enregistreEcBandeSinges(BandeDeSingesErratiquesEcouteur ecouteur)
	{
		this.bandeSingesEcouteurs.add(BandeDeSingesErratiquesEcouteur.class, ecouteur);
	}

	@Override
	public void run() 
	{
		//TODO
		final int dodo = 500;
		while (this.isAlive()) {
			for (int i = 0; i < this.erratiques.size(); i++) {
				final SingeErratique singeErratique = this.erratiques.get(i);
				singeErratique.deplacerSinge();
				this.bandeSingesEcouteurs.getListeners(BandeDeSingesErratiquesEcouteur.class)[0]
						.deplacementSingeErratique(i, singeErratique.getX(), singeErratique.getY());
			}
			try {
				Thread.sleep(dodo);
			}
			catch (InterruptedException e) {
				System.err.println("Une erreur dans le thread des singes est survenu " + e.toString());
			}
		}
	}

}

package tinymonkeys.modele;


/**
 * Classe abstraite AbstractSinge.
 * 
 * @version 1.0
 * @author Camille Constant
 */
public abstract class AbstractSinge extends AbstractElement
{
	/**
	 * Ile contenant tous les elements de celle-ci.
	 */
	private Ile monkeyIsland;

	/**
	 * Constructeur de la classe AbstractSinge.
	 *
	 * @param x la coordonnee en abscisse du singe.
	 * @param y la coordonnee en ordonnee du singe.
	 * @param ile l'ile sur laquelle vit le singe.
	 */
	public AbstractSinge(int x, int y, Ile ile)
	{
		super(x, y);
		this.monkeyIsland = ile;
	}

	/**
	 * Methode de deplacement du singe.
	 * Le deplacement est propre au type de singe (erratique, chasseur...).
	 */
	public abstract void deplacerSinge();

	/**
	 * Accesseur en lecture de l'ile.
	 * @return ile
	 */
	public Ile getMonkeyIsland()
	{
		return this.monkeyIsland;
	}

	/**
	 * Accesseur en écriture de l'île.
	 * @param monkeyIsland l'ile
	 */
	public void setMonkeyIsland(Ile monkeyIsland)
	{
		this.monkeyIsland = monkeyIsland;
	}
}

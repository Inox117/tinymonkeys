package tinymonkeys.modele;


import java.util.Random;

/**
 * Classe du singe erratique.
 * 
 * @version 1.0
 * @author Camille Constant
 *
 */

public class SingeErratique extends AbstractSinge
{
	/**
	 * Constructeur de la classe SingeErratique.
	 * 
	 * @param x la coordonnee en abscisse du singe.
	 * @param y la coordonnee en ordonnee du singe.
	 * @param ile l'ile sur laquelle vit le singe.
	 */
	public SingeErratique(int x, int y, Ile ile)
	{
		super(x, y, ile);
	}

	/**
	 * Constante de déplacement pour déterminer un mouvement.
	 */
	private static int constanteDeplacement = 3;

	/**
	 * Deplacement aleatoire du singe erratique.
	 */
	public void deplacerSinge()
	{
//		final Ile ile = this.getMonkeyIsland();
		int dx;
		int dy;
		do {
			// On fait - 1 pour être entre -1 et 1
			dx = this.chooseDx();
			// On autorise le déplacement sur l'axe y que si dx vaut 0 car
			// un singe ne peut se déplacer que verticalement ou horizontalement
			dy = this.chooseDy(dx);
		} while (!this.validationDeplacement(dx,dy));
		this.setPosition(this.getX() + dx , this.getY() + dy);
		this.gererPositionAvecPirate();
	}

	/**
	 * On valide le deplacement demandé
	 * en vérifiant dx et dy
	 * en vérifiant la position du singe avec les les autres singes
	 * en vérifiant que la case demandé est bien une terre
	 * @param dx x de déplacement
	 * @param dy y de déplacement
	 * @return autorisation de déplacement
	 */
	public boolean validationDeplacement(int dx, int dy) {
		return this.gererPositionAvecAutreSinge(this.getX() + dx, this.getY() + dy)
				&& this.validateDx(dx) && this.validateDy(dy)
				&& this.getMonkeyIsland().isCaseTerre(this.getX() + dx, this.getY() + dy);
	}

	/**
	 * Vérifie que dx à une valeur conforme aux besoins.
	 * @param dx valeur de x
	 * @return conformitée
	 */
	private boolean validateDx(int dx)
	{
		return ((this.getX() + dx) >= 0) && ((this.getX() + dx) <= this.getMonkeyIsland().getLargeurCarte());
	}

	/**
	 * Vérifie que dy à une valeur conforme aux besoins.
	 * @param dy valeur de y
	 * @return conformitée
	 */
	private boolean validateDy(int dy)
	{
		return ((this.getY() + dy) >= 0) && ((this.getY() + dy) <= this.getMonkeyIsland().getLargeurCarte());
	}

	/**
	 * choisis une direction horizontale.
	 * @return direction
	 */
	private int chooseDx()
	{
		final Random randX = new Random();
		return randX.nextInt(getConstanteDeplacement()) - 1;
	}

	/**
	 * choisis une direction verticale si on ne bouge pas horizontalement.
	 * @param dx direction horizontale
	 * @return direction
	 */
	private int chooseDy(int dx)
	{
		final Random randY = new Random();
		int dy = 0;
		if (dx == 0) {
			dy = randY.nextInt(getConstanteDeplacement()) - 1;
		}
		return dy;
	}

	/**
	 * Vérifie qu'un singe n'est pas déjà présent sur la case ciblée.
	 * @param x abscisse de la case ciblée
	 * @param y ordonnée de la case ciblée
	 * @return liberté de la case
	 */
	private boolean gererPositionAvecAutreSinge(int x, int y)
	{
		boolean libre = true;
		for (SingeErratique singe : this.getMonkeyIsland().getSingesErratiques().getSingesErratiques()) {
			if (singe.coordonneesEgales(x, y)) {
				libre = false;
			}
		}
		return libre;
	}

	/**
	 * Vérifie si un singe est allé manger le pirate.
	 */
	private void gererPositionAvecPirate()
	{
		boolean manger = false;
		final Ile ile = this.getMonkeyIsland();
		for (SingeErratique singe : ile.getSingesErratiques().getSingesErratiques()) {
			if (singe.coordonneesEgales(ile.getPirate().getX(), ile.getPirate().getY())) {
				manger = true;
				break;
			}
		}
        if(manger){
            ile.getPirate().informerMort();
        }
	}

	/**
	 * Accesseur en lecture de Constante Deplacement.
	 * @return constanteDeplacement
	 */
	public static int getConstanteDeplacement()
	{
		return constanteDeplacement;
	}

}

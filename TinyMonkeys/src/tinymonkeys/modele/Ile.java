package tinymonkeys.modele;

import java.util.Random;
import javax.swing.event.EventListenerList;

/**
 * Classe Ile. 
 * 
 * @version 1.0
 * @author Camille Constant, Quentin Victoor
 *
 */
public class Ile 
{
	/**
	 * La carte de l'ile : une matrice indiquant si chaque case est de type mer (0) ou terre (1).
	 */
	private int[][] carte;
	
	/**
	 * Les singes erratiques.
	 */
	private BandeDeSingesErratiques erratiques;
	
	/**
	 * Le tresor.
	 */
	private Tresor tresor;
	
	/**
	 * Le pirate du joueur.
	 */
	private Pirate pirate;
	
	/**
	 * Liste des écouteurs sur l'ile.
	 */
	final private EventListenerList ileEcouteurs;
	
	/**
	 * Constructeur de la classe Ile. 
	 */
	public Ile()
	{
		this.carte = null;
		this.erratiques = new BandeDeSingesErratiques(this);
		this.tresor = null;
		this.pirate = new Pirate(this);
		this.ileEcouteurs = new EventListenerList();
	}
	
	
	/**
	 * Indique la largeur de la carte en nombre de cases.
	 * 
	 * @return la largeur de la carte.
	 */
	public int getLargeurCarte()
	{
		return this.carte.length;
	}
	
	/**
	 * Indique la longueur de la carte en nombre de cases.
	 * 
	 * @return la longueur de la carte.
	 */
	public int getLongueurCarte()
	{
		return this.carte[0].length;
	}
	
	/**
	 * Permet l'acces en lecture a la valeur de la carte aux coordonnees indiquees.
	 * 
	 * @param x la coordonnee en abscisse.
	 * @param y la coordonnee en ordonnee.
	 * @return la valeur de la case de la carte aux coordonnees indiquees.
	 */
	public int valeurCarte(int x, int y)
	{
		return this.carte[x][y];
	}
	
	/**
	 * Creation de la carte.
	 * 
	 * @param carte la matrice terre-mer.
	 */
	public void creationCarte(int[][] carte)
	{
		//TODO
		this.setCarte(carte);
		// Longeur de la matrice
		final int longueur = this.getLongueurCarte();
		// Largeur de la matrice
		final int largeur = this.getLargeurCarte();
		// abscisse du point modifié
		int x;
		// ordonnée du point modifié
		int y;
		// nombre aléatoire
		final Random random = new Random();
		// On définit quel type de terrains doivent avoir les cases
		for (x = 1; x < longueur; x++) {
			for (y = 1; y < largeur; y++) {
				this.carte[x][y] = random.nextInt(2);
			}
		}
		for (x = 0; x < longueur; x++) {
			carte[x][0] = 0;
			carte[x][largeur - 1] = 0;
		}
		for (y = 0; y < largeur; y++) {
			carte[0][y] = 0;
			carte[longueur - 1][y] = 0;
		}
		// Une fois que le squelette de la carte est crée on vérifie que toute l'île est accessible
		// afin d'éviter des zones inaccessible mais indispensable
		this.affinageCarte();
		final IleEcouteur ileEcouteur = this.ileEcouteurs.getListeners(IleEcouteur.class)[0];
		ileEcouteur.creationCarte(this.carte);
	}

	/**
	 * Fonction pour affiner la carte et éviter des zones inaccessible.
	 */
	private void affinageCarte()
	{
		// Longeur de la matrice
		final int longueur = this.getLongueurCarte();
		// Largeur de la matrice
		final int largeur = this.getLargeurCarte();
		// abscisse du point modifié
		int x;
		// ordonnée du point modifié
		int y;
		// On vérifie que l'on as pas d'île à l'intérieur de l'île
		for (x = 0; x < longueur; x++) {
			for (y = 0; y < largeur; y++) {
				if (this.isAnIsland(x, y)) {
					this.carte[x][y] = 0;
				}
			}
		}
		// On s'assure que tous points de l'île peut être atteins
		// pour celà on relie les différentes relié en diagonale
		this.mettreDiagonaleAJour();
	}

	/**
	 * Méthode permettant de savoir si la case de terre analysé est entourée d'eau.
	 * @param x l'abscisse du point
	 * @param y l'ordonnée du point
	 * @return boolean indiquant si la terre est une île
	 */
	private boolean isAnIsland(int x, int y)
	{
		boolean isAnIsland = true;
		if ((x - 1) >= 0 && this.isCaseTerre(x - 1, y)) {
			isAnIsland = false;
		} else if ((x + 1) < this.getLargeurCarte() && this.isCaseTerre(x + 1, y)) {
			isAnIsland = false;
		} else if ((y - 1) >= 0 && this.isCaseTerre(x, y - 1)) {
			isAnIsland = false;
		} else if ((y + 1) < this.getLongueurCarte() && this.isCaseTerre(x, y + 1)) {
			isAnIsland = false;
		}
		return isAnIsland;
	}

	/**
	 * Méthode pour relier les différents points relié en diagonale.
	 */
	private void mettreDiagonaleAJour()
	{
		// Longeur de la matrice
		final int longueur = this.getLongueurCarte();
		// Largeur de la matrice
		final int largeur = this.getLargeurCarte();
		// abscisse du point modifié
		int x;
		// ordonnée du point modifié
		int y;
		for (x = 2; x < longueur - 2; x++) {
			for (y = 2; y < largeur - 2; y++) {
				this.checkDiagonaleGaucheHaut(x, y);
				this.checkDiagonaleGaucheBas(x, y);
				this.checkDiagonaleDroiteHaut(x, y);
				this.checkDiagonaleDroiteBas(x, y);
			}
		}
	}

	/**
	 * Vérifie si le point à besoin d'une diagonale droite bas et la met en place.
	 * @param x abscisse du point analysé
	 * @param y ordonnée du point analysé
	 */
	private void checkDiagonaleDroiteBas(int x, int y)
	{
		final int droiteBas = this.carte[x + 1][y + 1];
		final int droite = this.carte[x + 1][y];
		final int bas = this.carte[x][y + 1];
		if (bas == 0 && droite == 0 && droiteBas == 1) {
			this.creerDiagonale(x + 1, y);
		}
	}

	/**
	 * Vérifie si le point à besoin d'une diagonale droite haut et la met en place.
	 * @param x abscisse du point analysé
	 * @param y ordonnée du point analysé
	 */
	private void checkDiagonaleDroiteHaut(int x, int y)
	{
		final int droiteHaut = this.carte[x + 1][y - 1];
		final int droite = this.carte[x + 1][y];
		final int haut = this.carte[x][y - 1];
		if (haut == 0 && droite == 0 && droiteHaut == 1) {
			this.creerDiagonale(x + 1, y);
		}
	}

	/**
	 * Vérifie si le point à besoin d'une diagonale gauche bas et la met en place.
	 * @param x abscisse du point analysé
	 * @param y ordonnée du point analysé
	 */
	private void checkDiagonaleGaucheBas(int x, int y)
	{
		final int bas = this.carte[x][y + 1];
		final int gauche = this.carte[x - 1][y];
		final int gaucheBas = this.carte[x - 1][y + 1];
		if (bas == 0 && gauche == 0 && gaucheBas == 1) {
			this.creerDiagonale(x - 1, y);
		}
	}

	/**
	 * Vérifie si le point à besoin d'une diagonale gauche haut et la met en place.
	 * @param x abscisse du point analysé
	 * @param y ordonnée du point analysé
	 */
	private void checkDiagonaleGaucheHaut(int x, int y)
	{
		final int haut = this.carte[x][y - 1];
		final int gauche = this.carte[x - 1][y];
		final int gaucheHaut = this.carte[x - 1][y - 1];
		if (haut == 0 && gauche == 0 && gaucheHaut == 1) {
			this.creerDiagonale(x, y - 1);
		}
	}

	/**
	 * Méthode pour relier 2 points lié par un sommet.
	 * @param xNewTerre abscisse de la nouvelle terre
	 * @param yNewTerre ordonnée de la nouvelle terre
	 */
	private void creerDiagonale(int xNewTerre, int yNewTerre)
	{
		this.carte[xNewTerre][yNewTerre] = 1;
	}

	/**
	 * Mise à jour de la carte.
	 * 
	 * @param carte la matrice terre-mer. 
	 */
	public void setCarte(int[][] carte)
	{
		//TODO
		this.carte = carte;
	}

	/**
	 * Accesseur en lecture de la carte de l'île.
	 *
	 * @return la carte
	 */
	public int[][] getCarte()
	{
		return this.carte;
	}
	
	/**
	 * Accesseur en lecture du pirate de l'ile.
	 * 
	 * @return le pirate.
	 */
	public Pirate getPirate()
	{
		return this.pirate;
	}
	
	/**
	 * Creation du pirate sur l'ile.
	 * 
	 * @param avatar le lien vers l'image du pirate.
	 */
	public void ajoutPirate(String avatar)
	{
		//TODO
		this.pirate.setAvatar(avatar);
		this.pirate.setPositionInitiale();
	}
	
	/**
	 * Methode permettant de faire la demande de deplacement du pirate. 
	 * Cette methode fait suite a un appui sur une fleche directionnelle du clavier.
	 * 
	 * @param dx la direction en abscisse.
	 * @param dy la direction en ordonnee.
	 */
	public void demandeDeplacementPirate(int dx, int dy)
	{
		this.pirate.demandeDeplacement(dx, dy);
	}
	

	
	/**
	 * Accesseur en lecture de la bande de singes erratiques.
	 * 
	 * @return la bande de singes erratiques.
	 */
	public BandeDeSingesErratiques getSingesErratiques()
	{
		return this.erratiques;
	}
	
	/**
	 * Ajout du nombre indique de singes erratiques dans la liste des singes erratiques.
	 * 
	 * @param n le nombre de singes erratiques a ajouter.
	 */
	public void ajoutSingesErratiques(int n)
	{
		this.erratiques.ajoutSingesErratiques(n);
	}
	
	
	/**
	 * Accesseur en lecture du tresor.
	 * 
	 * @return le tresor.
	 */
	public Tresor getTresor()
	{
		return this.tresor;
	}
	
	/**
	 * Creation du tresor a une position aleatoire.
	 */
	public void creationTresor()
	{		
		//TODO
		final Random randX = new Random();
		final Random randY = new Random();
		int x;
		int y;
		do {
			x = randX.nextInt(this.getLargeurCarte());
			y = randY.nextInt(this.getLongueurCarte());
		} while (!this.isCaseTerre(x, y));
		this.tresor = new Tresor(x, y);
		for (IleEcouteur e: this.ileEcouteurs.getListeners(IleEcouteur.class)) {
			e.creationTresor(x, y);
		}

	}
	
	
	/**
	 * Suppression du tresor.  
	 */ 
	public void suppressionTresor()
	{		
		//TODO
		this.tresor = null;
		for (IleEcouteur e: this.ileEcouteurs.getListeners(IleEcouteur.class)) {
			e.suppressionTresor();
		}
	}


	/**
	 * Enregistre dans la liste des ecouteurs de l'ile l'ecouteur passe en parametre.
	 * @param ecouteur ecouteur de l'ile.
	 */
	public void enregistreEcIle(IleEcouteur ecouteur)
	{
		this.ileEcouteurs.add(IleEcouteur.class, ecouteur);
	}

	/**
	 * Vérifie que la case demandé est bien de la terre.
	 * @param x x
	 * @param y y
	 */
	public boolean isCaseTerre(int x, int y)
	{
		return this.carte[x][y] == 1;
	}

	/**
	 * Setter de la bande de singe erratiques.
	 * @param erratiques bande de singes
	 */
	public void setErratiques(BandeDeSingesErratiques erratiques)
	{
		this.erratiques = erratiques;
	}
}
